package br.com.tokiomarine.seguradora.avaliacao.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import br.com.tokiomarine.seguradora.avaliacao.entidade.Estudante;
import br.com.tokiomarine.seguradora.avaliacao.repository.EstudanteRepository;

// TODO Efetue a implementação dos métodos da classe service
@Service
public class EstudanteServiceImpl implements EstudandeService {
	
	 @Autowired
	 private JdbcTemplate conexao;

	EstudanteRepository repository;

	@Override
	public void cadastrarEstudante(@Valid Estudante estudante) {
		
		String sql = "insert into tbl_estudante (nome, email, matricula, telefone, curso) values (?, ?, ?, ?, ?)";
        Object[] params = new Object[] {
        		estudante.getNome(), 
        		estudante.getEmail(), 
        		estudante.getMatricula(), 
        		estudante.getTelefone(),
        		estudante.getCurso()
        		};

        conexao.update(sql, params);

	}

	@Override
	public void atualizarEstudante(@Valid Estudante estudante) {
		
		String sql = "update tbl_estudante set nome = ?, email = ?, matricula = ?, telefone = ?, curso = ? where id = ?";
        Object[] params = new Object[] {
        		estudante.getNome(), 
        		estudante.getEmail(), 
        		estudante.getMatricula(), 
        		estudante.getTelefone(),
        		estudante.getCurso(),
        		estudante.getId()
        		};

        conexao.update(sql, params);

	}
	
	@Override
	public void apagarEstudante(@Valid Estudante estudante) {
		
		String sql = "DELETE FROM tbl_estudante where id = ?";

		conexao.update(sql, new Object[]{estudante.getId()});
        
		//throw new IllegalArgumentException("Identificador inválido:" + id);
	}

	@Override
	public List<Estudante> buscarEstudantes() {
		String sql = "SELECT * FROM tbl_estudante";

        return conexao.query(sql, new BeanPropertyRowMapper<>(Estudante.class));
	}

	@Override
	public Estudante buscarEstudante(long id) {
		
		String sql = "SELECT * FROM tbl_estudante where id = ?";

		return conexao.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<>(Estudante.class));
        
		//throw new IllegalArgumentException("Identificador inválido:" + id);
	}

}
